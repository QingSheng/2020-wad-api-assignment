"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _dotenv = _interopRequireDefault(require("dotenv"));

var _express = _interopRequireDefault(require("express"));

var _movies = _interopRequireDefault(require("./api/movies"));

var _topratedMovies = _interopRequireDefault(require("./api/topratedMovies"));

var _upcomingMovies = _interopRequireDefault(require("./api/upcomingMovies"));

var _persons = _interopRequireDefault(require("./api/persons"));

var _seedData = require("./seedData");

var _genres = _interopRequireDefault(require("./api/genres"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

require("./db");

var _users = _interopRequireDefault(require("./api/users"));

var _expressSession = _interopRequireDefault(require("express-session"));

var _authenticate = _interopRequireDefault(require("./authenticate"));

var _loglevel = _interopRequireDefault(require("loglevel"));

//ca
//exercise
_dotenv["default"].config(); //test add


if (process.env.NODE_ENV === 'test') {
  _loglevel["default"].setLevel('warn');
} else {
  _loglevel["default"].setLevel('info');
}

if (process.env.SEED_DB === 'true' && process.env.NODE_ENV === 'development') {
  (0, _seedData.loadUsers)();
} // test end
// eslint-disable-next-line no-unused-vars


var errHandler = function errHandler(err, req, res, next) {
  /* if the error in development then send stack trace to display whole error,
  if it's in production then just send error message  */
  // eslint-disable-next-line no-undef
  if (process.env.NODE_ENV === 'production') {
    return res.status(500).send("Something went wrong!");
  }

  res.status(500).send("Hey!! You caught the error \uD83D\uDC4D\uD83D\uDC4D, ".concat(err.stack, " "));
};

var app = (0, _express["default"])(); // eslint-disable-next-line no-undef

var port = process.env.PORT; // eslint-disable-next-line no-undef

if (process.env.SEED_DB) {
  (0, _seedData.loadUsers)();
  (0, _seedData.loadMovies)();
  (0, _seedData.loadPersons)();
  (0, _seedData.loadTopratedMovies)();
  (0, _seedData.loadUpcomingMovies)();
} //session middleware


app.use((0, _expressSession["default"])({
  secret: 'ilikecake',
  resave: true,
  saveUninitialized: true
}));
app.use(_authenticate["default"].initialize());
app.use(_bodyParser["default"].json());
app.use(_bodyParser["default"].urlencoded());
app.use(_express["default"]["static"]('public'));
app.use('/api/movies', _movies["default"]); //delete passport.authenticate('jwt', {session: false}), 

app.use('/api/toprated', _topratedMovies["default"]);
app.use('/api/upcoming', _upcomingMovies["default"]);
app.use('/api/genres', _genres["default"]); //exercise

app.use('/api/persons', _persons["default"]); //ca
//Users router

app.use('/api/users', _users["default"]);
app.use(errHandler); //update /api/Movie route
// app.listen(port, () => {
//   console.info(`Server running at ${port}`);
// });

var server = app.listen(port, function () {
  _loglevel["default"].info("Server running at ".concat(port));
});
module.exports = server;