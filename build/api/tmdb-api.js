"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPersonMovie_credits = exports.getPopulerPerson = exports.getPerson = exports.getTopratedMovies = exports.getUpcomingMovies = exports.getSimilarMovies = exports.getMovieReviews = exports.getGenres = exports.getMovie = exports.getMovies = void 0;

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var getMovies = function getMovies() {
  return (0, _nodeFetch["default"])( // eslint-disable-next-line no-undef
  "https://api.themoviedb.org/3/discover/movie?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&include_adult=false&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getMovies = getMovies;

var getMovie = function getMovie(id) {
  return (0, _nodeFetch["default"])( // eslint-disable-next-line no-undef
  "https://api.themoviedb.org/3/movie/".concat(id, "?api_key=").concat(process.env.TMDB_KEY)).then(function (res) {
    return res.json();
  });
};

exports.getMovie = getMovie;

var getGenres = function getGenres() {
  return (0, _nodeFetch["default"])( // eslint-disable-next-line no-undef
  "https://api.themoviedb.org/3/genre/movie/list?api_key=".concat(process.env.TMDB_KEY, "&language=en-US")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.genres;
  });
};

exports.getGenres = getGenres;

var getMovieReviews = function getMovieReviews(id) {
  return (0, _nodeFetch["default"])( // eslint-disable-next-line no-undef
  "https://api.themoviedb.org/3/movie/".concat(id, "/reviews?api_key=").concat(process.env.TMDB_KEY)).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getMovieReviews = getMovieReviews;

var getSimilarMovies = function getSimilarMovies(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/similar?api_key=").concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getSimilarMovies = getSimilarMovies;

var getUpcomingMovies = function getUpcomingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/upcoming?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&include_adult=false&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getUpcomingMovies = getUpcomingMovies;

var getTopratedMovies = function getTopratedMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/top_rated?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getTopratedMovies = getTopratedMovies;

var getPerson = function getPerson(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "?api_key=ac30d257ab5fd18bd93513cf9e6e27b9&language=en-US")).then(function (res) {
    return res.json();
  });
};

exports.getPerson = getPerson;

var getPopulerPerson = function getPopulerPerson() {
  return (0, _nodeFetch["default"])(" https://api.themoviedb.org/3/person/popular?api_key=".concat(process.env.REACT_APP_TMDB_KEY, "&language=en-US&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getPopulerPerson = getPopulerPerson;

var getPersonMovie_credits = function getPersonMovie_credits(id) {
  return (0, _nodeFetch["default"])(" \n      https://api.themoviedb.org/3/person/".concat(id, "/movie_credits?api_key=ac30d257ab5fd18bd93513cf9e6e27b9&language=en-US")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.cast;
  });
};

exports.getPersonMovie_credits = getPersonMovie_credits;