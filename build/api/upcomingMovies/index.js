"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _tmdbApi = require("../tmdb-api");

var _upcomingModel = _interopRequireDefault(require("./upcomingModel"));

// ca add 
var router = _express["default"].Router(); // eslint-disable-next-line no-unused-vars


router.get('/', function (req, res, next) {
  _upcomingModel["default"].find().then(function (upcomingMovies) {
    return res.status(200).send(upcomingMovies);
  })["catch"](next);
}); // eslint-disable-next-line no-unused-vars

router.get('/:id', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getMovie)(id).then(function (upcomingMovie) {
    return res.status(200).send(upcomingMovie);
  })["catch"](function (error) {
    return next(error);
  });
}); // eslint-disable-next-line no-unused-vars

router.get('/:id/reviews', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getMovieReviews)(id).then(function (upcomingMoviesreviews) {
    return res.status(200).send(upcomingMoviesreviews);
  })["catch"](function (error) {
    return next(error);
  });
});
var _default = router;
exports["default"] = _default;