"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _tmdbApi = require("../tmdb-api");

var router = _express["default"].Router(); // eslint-disable-next-line no-unused-vars


router.get('/', function (req, res, next) {
  (0, _tmdbApi.getGenres)().then(function (movies) {
    return res.status(200).send(movies);
  })["catch"](function (error) {
    return next(error);
  });
});
var _default = router;
exports["default"] = _default;