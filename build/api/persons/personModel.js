"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose["default"].Schema;
var PersonSchema = new Schema({
  adult: {
    type: Boolean
  },
  gender: {
    type: Number
  },
  id: {
    type: Number,
    required: true,
    unique: true
  },
  known_for_department: {
    type: String
  },
  name: {
    type: String
  },
  popularity: {
    type: Number
  },
  profile_path: {
    type: String
  }
});

PersonSchema.statics.findByPersonDBId = function (id) {
  return this.findOne({
    id: id
  });
};

var _default = _mongoose["default"].model('Persons', PersonSchema);

exports["default"] = _default;