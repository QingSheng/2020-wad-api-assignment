"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _express = _interopRequireDefault(require("express"));

var _tmdbApi = require("../tmdb-api");

var _personModel = _interopRequireDefault(require("./personModel"));

// ca add 
var router = _express["default"].Router(); // eslint-disable-next-line no-unused-vars


router.get('/', function (req, res, next) {
  _personModel["default"].find().then(function (persons) {
    return res.status(200).send(persons);
  })["catch"](next);
}); // eslint-disable-next-line no-unused-vars

router.get('/:id', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getPerson)(id).then(function (person) {
    return res.status(200).send(person);
  })["catch"](function (error) {
    return next(error);
  });
}); // eslint-disable-next-line no-unused-vars

router.get('/:id/movie_credits', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getPersonMovie_credits)(id).then(function (credits) {
    return res.status(200).send(credits);
  })["catch"](function (error) {
    return next(error);
  });
});
module.exports = router;