"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = _interopRequireDefault(require("express"));

var _tmdbApi = require("../tmdb-api");

var _topratedModel = _interopRequireDefault(require("./topratedModel"));

// ca add 
var router = _express["default"].Router(); // eslint-disable-next-line no-unused-vars


router.get('/', function (req, res, next) {
  _topratedModel["default"].find().then(function (topratedMovies) {
    return res.status(200).send(topratedMovies);
  })["catch"](next);
}); // eslint-disable-next-line no-unused-vars

router.get('/:id', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getMovie)(id).then(function (topratedMovie) {
    return res.status(200).send(topratedMovie);
  })["catch"](function (error) {
    return next(error);
  });
}); // eslint-disable-next-line no-unused-vars

router.get('/:id/reviews', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getMovieReviews)(id).then(function (topratedMoviesreviews) {
    return res.status(200).send(topratedMoviesreviews);
  })["catch"](function (error) {
    return next(error);
  });
}); // eslint-disable-next-line no-unused-vars

router.get('/:id/similar', function (req, res, next) {
  var id = parseInt(req.params.id);
  (0, _tmdbApi.getSimilarMovies)(id).then(function (similarMovies) {
    return res.status(200).send(similarMovies);
  })["catch"](function (error) {
    return next(error);
  });
}); //ca

var _default = router;
exports["default"] = _default;