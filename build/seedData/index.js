"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadUsers = loadUsers;
exports.loadMovies = loadMovies;
exports.loadTopratedMovies = loadTopratedMovies;
exports.loadUpcomingMovies = loadUpcomingMovies;
exports.loadPersons = loadPersons;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _userModel = _interopRequireDefault(require("../api/users/userModel"));

var _movieModel = _interopRequireDefault(require("../api/movies/movieModel"));

var _topratedModel = _interopRequireDefault(require("../api/topratedMovies/topratedModel"));

var _upcomingModel = _interopRequireDefault(require("../api/upcomingMovies/upcomingModel"));

var _personModel = _interopRequireDefault(require("../api/persons/personModel"));

var _persons = require("./persons.js");

var _movies = require("./movies.js");

var _toprated = require("./toprated.js");

var _upcoming = require("./upcoming.js");

var users = [{
  'username': 'user1',
  'password': 'test1'
}, {
  'username': 'user2',
  'password': 'test2'
}]; // deletes all user documents in collection and inserts test data

function loadUsers() {
  return _loadUsers.apply(this, arguments);
} // deletes all movies documents in collection and inserts test data


function _loadUsers() {
  _loadUsers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log('load user Data');
            _context.prev = 1;
            _context.next = 4;
            return _userModel["default"].deleteMany();

          case 4:
            _context.next = 6;
            return users.forEach(function (user) {
              return _userModel["default"].create(user);
            });

          case 6:
            console.info("".concat(users.length, " users were successfully stored."));
            _context.next = 12;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](1);
            console.error("failed to Load user Data: ".concat(_context.t0));

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 9]]);
  }));
  return _loadUsers.apply(this, arguments);
}

function loadMovies() {
  return _loadMovies.apply(this, arguments);
}

function _loadMovies() {
  _loadMovies = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            console.log('load seed data');
            console.log(_movies.movies.length);
            _context2.prev = 2;
            _context2.next = 5;
            return _movieModel["default"].deleteMany();

          case 5:
            _context2.next = 7;
            return _movieModel["default"].collection.insertMany(_movies.movies);

          case 7:
            console.info("".concat(_movies.movies.length, " Movies were successfully stored."));
            _context2.next = 13;
            break;

          case 10:
            _context2.prev = 10;
            _context2.t0 = _context2["catch"](2);
            console.error("failed to Load movie Data: ".concat(_context2.t0));

          case 13:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 10]]);
  }));
  return _loadMovies.apply(this, arguments);
}

function loadTopratedMovies() {
  return _loadTopratedMovies.apply(this, arguments);
}

function _loadTopratedMovies() {
  _loadTopratedMovies = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3() {
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            console.log('load top rated movies data');
            console.log(_toprated.topratedMovies.length);
            _context3.prev = 2;
            _context3.next = 5;
            return _topratedModel["default"].deleteMany();

          case 5:
            _context3.next = 7;
            return _topratedModel["default"].collection.insertMany(_toprated.topratedMovies);

          case 7:
            console.info("".concat(_toprated.topratedMovies.length, " Top rated Movies were successfully stored."));
            _context3.next = 13;
            break;

          case 10:
            _context3.prev = 10;
            _context3.t0 = _context3["catch"](2);
            console.error("failed to Load top rated movie Data: ".concat(_context3.t0));

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[2, 10]]);
  }));
  return _loadTopratedMovies.apply(this, arguments);
}

function loadUpcomingMovies() {
  return _loadUpcomingMovies.apply(this, arguments);
} // deletes all movies documents in collection and inserts test data


function _loadUpcomingMovies() {
  _loadUpcomingMovies = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4() {
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            console.log('load upcoming movies data');
            console.log(_upcoming.upcomingMovies.length);
            _context4.prev = 2;
            _context4.next = 5;
            return _upcomingModel["default"].deleteMany();

          case 5:
            _context4.next = 7;
            return _upcomingModel["default"].collection.insertMany(_upcoming.upcomingMovies);

          case 7:
            console.info("".concat(_upcoming.upcomingMovies.length, " Upcoming Movies were successfully stored."));
            _context4.next = 13;
            break;

          case 10:
            _context4.prev = 10;
            _context4.t0 = _context4["catch"](2);
            console.error("failed to Load upcoming movie Data: ".concat(_context4.t0));

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[2, 10]]);
  }));
  return _loadUpcomingMovies.apply(this, arguments);
}

function loadPersons() {
  return _loadPersons.apply(this, arguments);
}

function _loadPersons() {
  _loadPersons = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5() {
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            console.log('load person data');
            console.log(_persons.persons.length);
            _context5.prev = 2;
            _context5.next = 5;
            return _personModel["default"].deleteMany();

          case 5:
            _context5.next = 7;
            return _personModel["default"].collection.insertMany(_persons.persons);

          case 7:
            console.info("".concat(_persons.persons.length, " Popular persons were successfully stored."));
            _context5.next = 13;
            break;

          case 10:
            _context5.prev = 10;
            _context5.t0 = _context5["catch"](2);
            console.error("failed to Load person Data: ".concat(_context5.t0));

          case 13:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[2, 10]]);
  }));
  return _loadPersons.apply(this, arguments);
}